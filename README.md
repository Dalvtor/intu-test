## Installation
Clone the repository with the command 
    
    git clone https://gitlab.com/Dalvtor/intu-test.git

## How to use
Create a virtual environment with the command

    virtualenv -p python3 env
    
If virtualenv is not installed it can be installed with pip

    pip install virtualenv

Activate the virtual environment

    source env/bin/activate

Navigate to project folder (where manage.py is), install the requirements and run the development server with the following commands

    pip install -r requirements.txt
    python manage.py runserver
    
The SQLite database provided is prepopulated with sample data. There are a few endpoints specified in the
endpoints section that provide an easy access to visualize that data.

More categories, retailers, retailer categories or category mappings can be created using the django admin.
Both username and password are: Test1234

## Available endpoints

##### There are a few endpoints just for ease of access to the data. Those are:

* List all the categories

        GET /api/categories/

* Get a category by id <int>

        GET /api/categories/<id>/
        
* List all the retailer categories

        GET /api/categories/retailers/
        
* Get a retailer category by id <int>

        GET /api/categories/retailers/<id>/
        
* List all the category mappings

        GET /api/categories/mappings/
        
        
##### The following endpoint returns the mapping of a retailer category to an internal category.

* Get a retailer category mapping. It accepts single or batch requests. 

        GET /api/categories/get_category_mapping/?<retailer_category_id>

    **Request parameters**

    retailer\_category_id: \<int>

    **Example with a single request**
    
        /api/categories/get_category_mapping?retailer_category_id=1

    **Example with batch request**

        /api/categories/get_category_mapping?retailer_category_id=1,2
        
##### The following endpoint creates a mapping between a retailer category and an internal category.        

* Create a mapping between retailer category and internal category

        POST /api/categories/create_category_mapping/

    **POST request parameters**

    retailer\_category_id: \<int>

    internal\_category\_id: \<int>




## Run tests
    python manage.py test

## Requirements
    Python 3

    django==1.11.9
    djangorestframework==3.7.7
    