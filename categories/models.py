from django.db import models
from django.template.defaultfilters import slugify
from django.utils.translation import gettext as _


class Category(models.Model):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', null=True, blank=True)
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        unique_together = ("name", "parent")
        verbose_name_plural = _("Categories")

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    @property
    def full_path(self):
        full_path = self.name
        parent = self.parent
        while parent:
            full_path = parent.name + "/" + full_path
            parent = parent.parent
        return full_path

    def __str__(self):
        return self.full_path


class Retailer(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class RetailerCategory(models.Model):
    retailer = models.ForeignKey(Retailer)
    name = models.CharField(max_length=255)
    slug = models.SlugField(null=True, blank=True)

    class Meta:
        verbose_name_plural = _("Retailer categories")

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(RetailerCategory, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class CategoryMapping(models.Model):
    retailer_category = models.ForeignKey(RetailerCategory)
    internal_category = models.ForeignKey(Category)

    def __str__(self):
        return "{}: {} -> {}".format(self.retailer_category.retailer, self.retailer_category, self.internal_category)



