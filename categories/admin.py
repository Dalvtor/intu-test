from django.contrib import admin
from .models import Category, Retailer, RetailerCategory, CategoryMapping

admin.site.register(Category)
admin.site.register(Retailer)
admin.site.register(RetailerCategory)
admin.site.register(CategoryMapping)
