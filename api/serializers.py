from rest_framework import serializers
from categories.models import Category, RetailerCategory, CategoryMapping


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ('id', 'name', 'parent', 'full_path', 'slug')


class RetailerCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = RetailerCategory
        fields = ('id', 'retailer', 'name', 'slug')


class CategoryMappingSerializer(serializers.ModelSerializer):

    class Meta:
        model = CategoryMapping
        fields = ('id', 'retailer_category', 'internal_category')
