from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from categories.models import Category, RetailerCategory, CategoryMapping, Retailer


class GetCategoryMappingTest(APITestCase):
    def test_mapping_no_parameters(self):
        """
        Test get category mapping without parameters, should return 400
        """
        url = reverse('api:get_category')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_mapping_incorrect_parameters(self):
        """
        Test get category mapping with incorrect parameters, should return 400
        """
        url = reverse('api:get_category')
        data = {'a': 1}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_mapping_incorrect_format_parameters(self):
        """
        Test get category mapping with wrong parameters format, should return 400
        """
        url = reverse('api:get_category')
        data = {'retailer_category_id': 'b'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_single_existing_mapping(self):
        """
        Test get a single valid category mapping, should return 200
        """
        url = reverse('api:get_category')
        internal_category = Category.objects.create(name="Internal test category")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category = RetailerCategory.objects.create(name="External test category", retailer=retailer)
        CategoryMapping.objects.create(internal_category=internal_category, retailer_category=retailer_category)
        data = {'retailer_category_id': '1'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_single_non_existing_mapping(self):
        """
        Test get a single category mapping that doesn't exist, should return 404
        """
        url = reverse('api:get_category')
        Category.objects.create(name="Internal test category")
        retailer = Retailer.objects.create(name="Test Retailer")
        RetailerCategory.objects.create(name="External test category", retailer=retailer)
        data = {'retailer_category_id': '1'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_non_existing_category(self):
        """
        Test get a single category mapping whose category doesn't exist, should return 404
        """
        url = reverse('api:get_category')
        internal_category = Category.objects.create(name="Internal test category")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category = RetailerCategory.objects.create(name="External test category", retailer=retailer)
        CategoryMapping.objects.create(internal_category=internal_category, retailer_category=retailer_category)
        data = {'retailer_category_id': '2'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_multiple_existing_mappings(self):
        """
        Test get multiple category mappings, should return 200
        """
        url = reverse('api:get_category')
        internal_category_1 = Category.objects.create(name="Internal test category 1")
        internal_category_2 = Category.objects.create(name="Internal test category 2")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category_1 = RetailerCategory.objects.create(name="External test category 1", retailer=retailer)
        retailer_category_2 = RetailerCategory.objects.create(name="External test category 2", retailer=retailer)
        CategoryMapping.objects.create(internal_category=internal_category_1, retailer_category=retailer_category_1)
        CategoryMapping.objects.create(internal_category=internal_category_2, retailer_category=retailer_category_2)
        data = {'retailer_category_id': '1,2'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_existing_and_non_existing_mappings(self):
        """
        Test get multiple category mappings, some exist and some not. Should return 200
        """
        url = reverse('api:get_category')
        internal_category_1 = Category.objects.create(name="Internal test category 1")
        internal_category_2 = Category.objects.create(name="Internal test category 2")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category_1 = RetailerCategory.objects.create(name="External test category 1", retailer=retailer)
        retailer_category_2 = RetailerCategory.objects.create(name="External test category 2", retailer=retailer)
        CategoryMapping.objects.create(internal_category=internal_category_1, retailer_category=retailer_category_1)
        CategoryMapping.objects.create(internal_category=internal_category_2, retailer_category=retailer_category_2)
        data = {'retailer_category_id': '1,2,3'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_multiple_non_existing_mappings(self):
        """
        Test get multiple category mappings. None exist. Should return 404
        """
        url = reverse('api:get_category')
        internal_category_1 = Category.objects.create(name="Internal test category 1")
        internal_category_2 = Category.objects.create(name="Internal test category 2")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category_1 = RetailerCategory.objects.create(name="External test category 1", retailer=retailer)
        retailer_category_2 = RetailerCategory.objects.create(name="External test category 2", retailer=retailer)
        CategoryMapping.objects.create(internal_category=internal_category_1, retailer_category=retailer_category_1)
        CategoryMapping.objects.create(internal_category=internal_category_2, retailer_category=retailer_category_2)
        data = {'retailer_category_id': '3,4'}
        response = self.client.get(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_incorrect_method_mapping(self):
        """
        Test get category mapping with POST request instead of GET, should return 405
        """
        url = reverse('api:get_category')
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class CreateCategoryMappingTest(APITestCase):
    def test_create_mapping_no_parameters(self):
        """
        Test create category mapping without parameters, should return 400
        """
        url = reverse('api:map_category')
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_mapping_incorrect_parameters(self):
        """
        Test create category mapping with incorrect parameters, should return 400
        """
        url = reverse('api:map_category')
        data = {'a': 1, 'b': 1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_mapping_incorrect_format_parameters(self):
        """
        Test create category mapping with wrong parameters format, should return 400
        """
        url = reverse('api:map_category')
        data = {'retailer_category_id': '', 'internal_category_id': 'b'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_mapping(self):
        """
        Test create a category mapping, should return 201
        """
        url = reverse('api:map_category')
        internal_category = Category.objects.create(name="Internal test category")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category = RetailerCategory.objects.create(name="External test category", retailer=retailer)
        data = {'retailer_category_id': retailer_category.id, 'internal_category_id': internal_category.id}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(CategoryMapping.objects.all().count(), 1)

    def test_create_an_already_existing_mapping(self):
        """
        Test creating a mapping for a category that already had one, should return 400
        """
        url = reverse('api:map_category')
        internal_category = Category.objects.create(name="Internal test category")
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category = RetailerCategory.objects.create(name="External test category", retailer=retailer)
        data = {'retailer_category_id': retailer_category.id, 'internal_category_id': internal_category.id}
        self.client.post(url, data, format='json')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(CategoryMapping.objects.all().count(), 1)

    def test_create_incorrect_internal_category_mapping(self):
        """
        Test creating a mapping for an internal category that doesn't exist, should return 404
        """
        url = reverse('api:map_category')
        retailer = Retailer.objects.create(name="Test Retailer")
        retailer_category = RetailerCategory.objects.create(name="External test category", retailer=retailer)
        data = {'retailer_category_id': retailer_category.id, 'internal_category_id': '1'}
        self.client.post(url, data, format='json')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_create_incorrect_external_category_mapping(self):
        """
        Test creating a mapping for an internal category that doesn't exist, should return 404
        """
        url = reverse('api:map_category')
        internal_category = Category.objects.create(name="Internal test category")
        data = {'retailer_category_id': '1', 'internal_category_id': internal_category.id}
        self.client.post(url, data, format='json')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_incorrect_method_create_mapping(self):
        """
        Test create category mapping with GET request instead of POST, should return 405
        """
        url = reverse('api:map_category')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

