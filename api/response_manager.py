class ResponseManager:

    @staticmethod
    def json_success_response(data, message):
        if message:
            return {'status': 'Success', 'data': data, 'message': message}
        return {'status': 'Success', 'data': data}

    @staticmethod
    def json_error_response(message):
        return {'status': 'Error', 'message': message}
